package com.humanbooster.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MysqlConnector {

    private static String url = "jdbc:mysql://localhost/jdbccourses";
    private static String user = "root";
    private static String passwd = "tiger";
    private static Connection connect;


    public static Connection getInstance(){
        if(connect == null){
            try {
                connect = DriverManager.getConnection(url, user, passwd);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connect;
    }


    public static void closeConnexion() throws SQLException {
        if(!connect.isClosed()){
            connect.close();
        }
    }
}

