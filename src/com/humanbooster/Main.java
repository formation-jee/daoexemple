package com.humanbooster;

import com.humanbooster.beans.User;
import com.humanbooster.dao.DAO;
import com.humanbooster.dao.implementation.UserDao;
import com.humanbooster.jdbc.MysqlConnector;

import java.sql.SQLException;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws SQLException {
        UserDao userDao = new UserDao();

        // Affichage de tous les utilisateurs
        ArrayList<User> users = userDao.getAll();
        for(User user: users) {
            System.out.println(user);
        }

        // Création d'un nouvel utilisateur
        User user = new User("Elon", "Musk","emusk@tesla.com");
        userDao.create(user);

        // Modification d'un utilisateur
        user.setEmail("emusk@twitter.com");
        userDao.update(user);

        // Suppression d'un utilisateur
        userDao.delete(user);

        // Fermeture de la connexion
        MysqlConnector.closeConnexion();
    }
}
