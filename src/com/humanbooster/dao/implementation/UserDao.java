package com.humanbooster.dao.implementation;

import com.humanbooster.beans.User;
import com.humanbooster.dao.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UserDao extends DAO<User> {
    @Override
    public ArrayList<User> getAll() {
        ArrayList<User> listUser = new ArrayList<User>();
        try {
            Statement statement = connect
                    .createStatement();

            ResultSet rs = statement.executeQuery("SELECT * FROM user;");

            while (rs.next()) {
                int id = rs.getInt("id");
                String firstname = rs.getString("firstname");
                String lastname = rs.getString("lastname");
                String email = rs.getString("email");
                listUser.add(new User(id, firstname, lastname, email));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listUser;
    }

    @Override
    public User find(long id) {
        User user = null;
        try {
            PreparedStatement preparedStatement = connect
                    .prepareStatement("select * from user where id = ?");
            preparedStatement.setLong(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                long idDb = rs.getLong("id");
                String firstname = rs.getString("firstname");
                String lastname = rs.getString("lastname");
                String email = rs.getString("email");

                user = new User(idDb, firstname, lastname, email);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;

    }

    @Override
    public User create(User obj) {
        try {
            PreparedStatement prepare = this.connect
                    .prepareStatement(
                            "INSERT INTO user (firstname, lastname, email) VALUES (?,?,?)",
                            Statement.RETURN_GENERATED_KEYS
                    );

            prepare.setString(1, obj.getFirstname());
            prepare.setString(2, obj.getLastname());
            prepare.setString(3, obj.getEmail());
            prepare.executeUpdate();

            ResultSet rs = prepare.getGeneratedKeys();

            if (rs.next()) {
                long id = rs.getLong(1);
                obj.setId(id); // display inserted record
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return obj;
    }

    @Override
    public User update(User obj) {
        try {
            PreparedStatement prepare = this.connect
                    .prepareStatement(
                            "UPDATE user SET firstname = ?, lastname = ?, email = ? WHERE id = ?",
                            Statement.RETURN_GENERATED_KEYS
                    );

            prepare.setString(1, obj.getFirstname());
            prepare.setString(2, obj.getLastname());
            prepare.setString(3, obj.getEmail());
            prepare.setLong(4, obj.getId());
            prepare.executeUpdate();

            ResultSet rs = prepare.getGeneratedKeys();

            if (rs.next()) {
                long id = rs.getLong(1);
                obj.setId(id); // display inserted record
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return obj;

    }

    @Override
    public void delete(User obj) {
        try {
            PreparedStatement prepare = this.connect
                    .prepareStatement(
                            "DELETE FROM user WHERE id = ?"
                    );

            prepare.setLong(1, obj.getId());
            prepare.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
