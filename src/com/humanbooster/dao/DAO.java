package com.humanbooster.dao;

import com.humanbooster.jdbc.MysqlConnector;
import java.sql.Connection;
import java.util.ArrayList;


public abstract class DAO<E> {

    protected Connection connect = MysqlConnector.getInstance();

    public abstract ArrayList<E> getAll();
    public abstract E find(long id);
    public abstract E create(E obj);
    public abstract E update(E obj);
    public abstract void delete(E obj);
}


